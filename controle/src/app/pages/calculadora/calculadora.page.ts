import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from 'src/app/services/calculadora.service';

@Component({
  selector: 'calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  form: FormGroup;
  titulo = "Calculadora";
  quociente: any = "";

  constructor(private builder: FormBuilder, private service: CalculadoraService) { }

  ngOnInit() {
    this.form = this.builder.group({
      dividendo: ["", [Validators.required]],
      divisor: ["", [Validators.required]]
    });
  }

  dividir() {
    const valores = this.form.value;
    this.quociente = this.service.dividir(valores.dividendo, valores.divisor);
  }

}
