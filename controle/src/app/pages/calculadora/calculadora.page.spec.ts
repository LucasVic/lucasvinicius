import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';

/**
 * Etapas de um teste:
 * 
 * arrange => prepara os dados
 * 
 * act => efetua os testes
 * 
 * assert => compara os resultados
 * 
 */

describe('A página Calculadora', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CalculadoraPage],
      imports: [IonicModule.forRoot(), ReactiveFormsModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um título', () => {
    expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o título', () => {
    const titulo = view.querySelector(".titulo").textContent;
    expect(titulo).toEqual("Calculadora");
  });

  it('deve estar com o botão de divisão desabilitado', () => {
    expect(model.form.invalid).toBeTrue();
  });

  it('deve validar o botão quando o usuário inserir valores', () => {
    model.form.controls.dividendo.setValue(9);
    model.form.controls.divisor.setValue(3);

    expect(model.form.valid).toBeTrue();
  });

  it('deve efetuar a divisão quando o usuário clicar no botão', () => {
    // arrange
    model.form.controls.dividendo.setValue(9);
    model.form.controls.divisor.setValue(3);

    //act
    const dividir = fixture.debugElement.query(By.css("#dividir"));
    dividir.triggerEventHandler("click", null);
    fixture.detectChanges();

    //assert
    const quociente = view.querySelector("#quociente").textContent;
    expect(quociente).toEqual('3');
  });
});
