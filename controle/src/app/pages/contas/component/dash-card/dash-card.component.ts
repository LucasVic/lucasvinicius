import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dash-card',
  templateUrl: './dash-card.component.html',
  styleUrls: ['./dash-card.component.scss'],
})
export class DashCardComponent implements OnInit {
  @Input() tipo;
  @Input() titulo;
  @Input() icone;
  @Input() cor;
  @Input() valor;
  @Input() num;

  constructor() { }

  ngOnInit() {
    switch (this.tipo) {
      case 'pagar': this.pagar(); break;
      case 'receber': this.receber(); break;
      case 'saldo': this.saldo(); break;
    }
  }

  pagar() {
    this.titulo = this.titulo ? this.titulo : "Contas a pagar";
    this.icone = this.icone ? this.icone : "arrow-down";
    this.cor = this.cor ? this.cor : "danger";
  }

  receber() {
    this.titulo = this.titulo ? this.titulo : "Contas a receber";
    this.icone = this.icone ? this.icone : "arrow-up";
    this.cor = this.cor ? this.cor : "success";
  }

  saldo() {
    this.titulo = this.titulo ? this.titulo : "Saldo da conta";
    this.icone = this.icone ? this.icone : "wallet";
    this.cor = this.cor ? this.cor : "primary";
  }

}
