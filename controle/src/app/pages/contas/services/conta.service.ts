import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { NavController } from '@ionic/angular';
import { map } from 'rxjs/operators'
import { DateHelper } from 'src/app/helpers/DateHelper';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  collection: AngularFirestoreCollection;

  constructor(private nav: NavController, private db: AngularFirestore) { }

  /**
   * Recebe os dados de uma conta e insere no banco
   * retorna para a pagina home
   * @param conta: objeto - dados da conta 
   */
  registrarConta(conta) {
    conta.id = this.db.createId();

    this.collection = this.db.collection("conta");
    this.collection.doc(conta.id).set(conta).
      then(() => this.nav.navigateBack("home"));
  }

  /**
   * Lista os dados das contas de um tipo especifico
   * ValueChanges() retorna sempre que os dados do banco forem atualizados
   * @param tipoConta: string - tipo da conta desejada
   * @returns Observable com array de contas daquele tipo
   */
  lista(tipoConta) {
    this.collection = this.db.collection("conta", ref => ref.where("tipo", "==", tipoConta));
    return this.collection.valueChanges();
  }

  /**
   * Remove uma conta especifica do banco a partir de seu id
   * @param conta: objeto - dados da conta
   */
  remove(conta) {
    this.collection = this.db.collection("conta");
    this.collection.doc(conta.id).delete();
  }

  /**
   * Edita os dados de uma conta especifica no banco a partir de su id
   * @param conta: objeto - dados da conta
   */
  edita(conta) {
    this.collection = this.db.collection("conta");
    this.collection.doc(conta.id).update(conta);
  }

  /**
   * Resgata os dados das contas de um tipo especifico
   * @param tipoConta: string - tipo da conta desejada
   * @returns Oservable com array de contas do tipo especificado
   */
  total(tipoConta, data) {
    const aux = DateHelper.breakDate(data);
    const mes = aux.mes;
    const ano = aux.ano;

    this.collection = this.db.collection("conta", ref => ref.where("tipo", "==", tipoConta).where("mes", "==", mes).where("ano", "==", ano));
    return this.collection.get().pipe(map(snap => {
      let count = 0;
      let soma = 0;

      snap.docs.map(doc => {
        const conta = doc.data();
        const valor = parseFloat(conta.valor);
        soma += valor;
        count++;
      });
      return { num: count, valor: soma }
    }));
  }
}
