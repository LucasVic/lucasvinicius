import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateHelper } from 'src/app/helpers/DateHelper';
import { ContaService } from '../services/conta.service';

@Component({
  selector: 'cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  contaForm: FormGroup;

  constructor(private builder: FormBuilder, private service: ContaService) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.contaForm = this.builder.group({
      vencimento: [new Date().toISOString(), [Validators.required]],
      parceiro: ["", [Validators.required, Validators.minLength(5)]],
      descricao: ["", [Validators.required, Validators.minLength(6)]],
      valor: ["", [Validators.required, Validators.min(0.01)]],
      tipo: ["", [Validators.required]]
    });
  }

  // Registra os dados recebidos no banco do Firebase na coleção  
  registrarConta() {
    let conta = this.contaForm.value;
    const data = this.contaForm.get("vencimento").value;

    conta = { ...conta, ...DateHelper.breakDate(data) }

    delete conta.vencimento;

    this.service.registrarConta(conta);
  }
}
