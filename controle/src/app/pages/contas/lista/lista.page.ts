import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ContaService } from '../services/conta.service';

@Component({
  selector: 'lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  listaContas;
  tipo;

  constructor(private service: ContaService, private alert: AlertController, private router: Router) { }

  ngOnInit() {
    const url = this.router.url;
    const tipoConta = url.split("/")[2];
    const stringTipoEditada = tipoConta.charAt(0).toUpperCase() + tipoConta.slice(1);

    this.tipo = stringTipoEditada;
    this.service.lista(tipoConta).
      subscribe(item => this.listaContas = item);
  }

  async remove(conta) {
    const confirm = await this.alert.create({
      header: "Remoção de conta",
      message: "Tem certeza que quer apagar essa conta?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Deletar",
          handler: () => this.service.remove(conta)
        }
      ]
    });

    confirm.present();
  }

  async edita(conta) {
    const editForm = await this.alert.create({
      header: "Edição de conta",
      message: "Altere os dados da conta nos campos a seguir:",
      inputs: [
        {
          name: "parceiro",
          placeholder: "Parceiro comercial",
          value: conta.parceiro
        },
        {
          name: "descricao",
          placeholder: "Descrição da conta",
          value: conta.descricao
        },
        {
          name: "valor",
          placeholder: "Valor da conta",
          type: "number",
          value: conta.valor
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Editar",
          handler: (data) => {
            // Espalhando os dados de `conta` e `data`
            // ... -> variavel de espalhamento
            const dadosEditados = { ...conta, ...data }
            this.service.edita(dadosEditados);
          }
        }
      ]
    })

    editForm.present();
  }
}
