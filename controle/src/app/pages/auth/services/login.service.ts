import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<firebase.User>;

  constructor(private nav: NavController, private toast: ToastController, private auth: AngularFireAuth) {
    this.isLoggedIn = auth.authState;
  }

  login(user, alreadyLogged = false) {
    if (alreadyLogged) {
      this.nav.navigateForward("home");
      return;
    }

    // Método .then() trabalha após o retorno de uma Promise, utilizando os dados que esta retorna.
    // Método .catch() captura possíveis erros ou retornos falsos que possam vir da Promisse
    this.auth.signInWithEmailAndPassword(user.email, user.password).
      then(() => this.nav.navigateForward("home")).
      catch(() => this.showError());

  }

  logout() {
    this.auth.signOut().
      then(() => this.nav.navigateBack("auth"));

  }

  createUser(user) {
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
      then(credentials => console.log(credentials));
  }

  recoverPass(email) {
    this.auth.sendPasswordResetEmail(email).
      then(() => this.nav.navigateBack("auth")).
      catch(err => {
        console.log(err);
      });
  }


  private async showError() {
    const toastCTRL = await this.toast.create({
      message: "Dados de acesso incorretos!",
      position: 'top',
      duration: 3000
    });

    toastCTRL.present();
  }
}
