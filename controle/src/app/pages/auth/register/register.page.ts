import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;

  constructor(private builder: FormBuilder, private service: LoginService) { }

  ngOnInit() {
    this.registerForm = this.builder.group({
      name: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(19)]],
      lastName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(19)]],
      email: ["", [Validators.email, Validators.required]],
      password: ["", [Validators.required, Validators.minLength(8)]],
      confirmPassword: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  createUser() {
    const user = this.registerForm.value;
    this.service.createUser(user);
  }

}
