import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TopoLoginComponent } from './component/topo-login/topo-login.component';
import { ForgotPage } from './forgot/forgot.page';
import { LoginPage } from './login/login.page';
import { RegisterPage } from './register/register.page';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LoginPage },
      { path: 'register', component: RegisterPage },
      { path: 'forgot', component: ForgotPage }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],

  declarations: [
    LoginPage,
    RegisterPage,
    ForgotPage,
    TopoLoginComponent
  ]
})
export class AuthRoutingModule { }
