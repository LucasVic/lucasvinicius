import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  /* 
    Inserção de dependências
    ** nav -> cuida da navegação entre páginas
    ** toast -> cuida das respostas e pop-ups de erro
    ** builder -> cuida da construção e validação dos campos de um formulário
  */
  constructor(private builder: FormBuilder, private services: LoginService) {
  }

  ngOnInit() {
    this.isUserLoggedIn();

    this.loginForm = this.builder.group({
      email: ["", [Validators.email, Validators.required]],
      password: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  isUserLoggedIn() {
    this.services.isLoggedIn.subscribe(user => {
      if (user) {
        // Usuário existe e já está logado
        this.services.login(user, true);
      }
    });
  }

  login() {
    // Os dados dos campos do form agora estão no grupo do formulário
    const user = this.loginForm.value;
    this.services.login(user);
  }
}
