import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
  forgotForm: FormGroup;

  constructor(private builder: FormBuilder, private service: LoginService) { }

  ngOnInit() {
    this.forgotForm = this.builder.group({
      email: ["", [Validators.email, Validators.required]]
    });
  }

  recoverPass() {
    const email = this.forgotForm.value.email;
    this.service.recoverPass(email);
  }

}
