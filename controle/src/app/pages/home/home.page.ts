import { Component, OnInit } from '@angular/core';
import { LoginService } from '../auth/services/login.service';
import { ContaService } from '../contas/services/conta.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  data = new Date().toISOString();

  dadosContas = {
    pagar: { num: 0, valor: "0" },
    receber: { num: 0, valor: "0" },
    saldo: { num: 0, valor: "0", cor: "" },
  };

  constructor(private Loginservices: LoginService, private ContaServices: ContaService) { }

  // ionViewWillEnter executa sempre q a pagina abre
  ionViewWillEnter() {
    this.atualizaContas();
  }

  atualizaContas() {
    this.ContaServices.total("pagar", this.data).subscribe(
      (x: any) => {

        this.dadosContas.pagar = x;

        this.ContaServices.total("receber", this.data).subscribe(
          (y: any) => {

            this.dadosContas.receber = y;
            this.atualizaDados();

          }
        );

      }
    );
  }

  atualizaDados() {
    this.dadosContas.saldo.num = this.dadosContas.pagar.num + this.dadosContas.receber.num;
    this.dadosContas.saldo.cor = (parseFloat(this.dadosContas.receber.valor) - parseFloat(this.dadosContas.pagar.valor)) > 0 ? "primary" : "warning";
    this.dadosContas.saldo.valor = (parseFloat(this.dadosContas.receber.valor) - parseFloat(this.dadosContas.pagar.valor)).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    this.dadosContas.pagar.valor = parseFloat(this.dadosContas.pagar.valor).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    this.dadosContas.receber.valor = parseFloat(this.dadosContas.receber.valor).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
  }

  // // ngOnInit executa na primeira vez q a pagina abre
  // ngOnInit() { }

  logout() {
    this.Loginservices.logout();
  }
}
