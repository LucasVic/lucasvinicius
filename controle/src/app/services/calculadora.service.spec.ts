import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('A classe Calculadora', () => {
  let service: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculadoraService);
  });

  it('deve realizar fatoriais', () => {
    expect(service.fatorial(5)).toBe(120);
  });

  describe('deve realizar divisões', () => {

    it('entre numeros inteiros', () => {
      expect(service.dividir(1, 2)).toBe(0.5);
    });

    it('entre numeros decimais', () => {
      expect(service.dividir(0.8, 0.72)).toBe(1.1111111111111112);
    });

    it('entre numeros negativos', () => {
      expect(service.dividir(-4, 2)).toBe(-2);
      expect(service.dividir(9, -3)).toBe(-3);
      expect(service.dividir(-68, -17)).toBe(4);
    });

    it('com exceção do zero', () => {
      expect(service.dividir(1, 0)).toEqual("erro");
      // asserção --> expectativa do que será retornado
    });

  });

});
