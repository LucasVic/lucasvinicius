import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  dividir(a, b) {
    if (b == 0)
      return "erro";

    return a / b;
  }

  fatorial(x) {
    if (x <= 1)
      return 1;

    return x * this.fatorial(x - 1);
  }
}
