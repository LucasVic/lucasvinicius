export class DateHelper {

    /**
     * Quebra uma string de data no formato ISO em um objeto com os valores de ano, mes e dia
     * 
     * @param date: string - data em formato ISO
     * @returns object - {ano: number, mes: number, dia: number}
     */
    static breakDate(date) {
        const ano_mes_dia = (date.split("T")[0]).split("-");

        return {
            ano: Number(ano_mes_dia[0]),
            mes: Number(ano_mes_dia[1]),
            dia: Number(ano_mes_dia[2])
        }
    }
}