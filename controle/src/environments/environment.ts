// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCG0wnQfrswY9nQHTppPtIXpHXSe9SK3Xg",
    authDomain: "controle-if-lv.firebaseapp.com",
    projectId: "controle-if-lv",
    storageBucket: "controle-if-lv.appspot.com",
    messagingSenderId: "887616834242",
    appId: "1:887616834242:web:f1a3fb87f1e61754b3095b",
    measurementId: "G-086XWKGFPB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
